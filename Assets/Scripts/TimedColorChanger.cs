﻿using UniRx;
using UnityEngine;

namespace Game
{
    public class TimedColorChanger : MonoBehaviour
    {
        private GeometryObjectModel geometryObjectModel;

        private CompositeDisposable disposables;

        private GameData gameData;

        private int seconds = 0;

        void Start()
        {
            gameData = Resources.Load<GameData>("GameData");

            geometryObjectModel = GetComponent<GeometryObjectModel>();

            seconds = gameData.ObsevableTime;

            Observable.Timer(System.TimeSpan.FromSeconds(seconds)) // создаем timer Observable
                .Repeat() // делает таймер циклическим
                .Subscribe(_ =>
                { // подписываемся
                SetRandomColor();
                }).AddTo(disposables); // привязываем подписку к disposable
        }


        void SetRandomColor()
        {
            geometryObjectModel?.SetColor(Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f));
        }

        void OnEnable()
        {  
            disposables = new CompositeDisposable(); // создаем disposable
        }

        void OnDisable()
        {   
            if (disposables != null)
            {
                disposables.Dispose(); // уничтожаем подписки
            }
        }
    }
}
