﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Game
{
    public class GeometryObjectModel : MonoBehaviour
    {
        [SerializeField]
        private int clickCount;
       
        private Color CubeColor;

        private GeometryObjectData objectData;

        private List<ClickColorData> clickColorData;

        private MeshRenderer meshRenderer;
        private void Start()
        {
            clickColorData = new List<ClickColorData>();
            
            meshRenderer = GetComponent<MeshRenderer>();

            objectData = Resources.Load<GeometryObjectData>("GeometryObjectData");

            clickColorData = objectData.ClickColorDatas
                                       .Where(c => c.ObjectType == gameObject.name).ToList();
        }


        public void SetClick()
        {
            clickCount++;

            foreach (var data in clickColorData)
            {
                if(clickCount >= data.MinClicksCount && clickCount <= data.MaxClicksCount)
                {
                    SetColor(data.Color);
                }
            }
        }

        public void SetColor(Color color)
        {
            meshRenderer.material.SetColor("_Color", color);
        }
    }
}
