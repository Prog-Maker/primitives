﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Game
{
    public class JsonObjectCreator : MonoBehaviour
    {
        [MenuItem("MyMenu/CreateJsonObject")]
        static void CreateJsonObject()
        {
            Primitive[] primitives = new Primitive[4];

            primitives[0] = new Primitive { Name = "Cube" };
            primitives[1] = new Primitive { Name = "Sphere" };
            primitives[2] = new Primitive { Name = "Capsule" };
            primitives[3] = new Primitive { Name = "Cylinder" };

            string json = JsonHelper.ToJson(primitives);

            Debug.Log(json);

            File.WriteAllText("Assets/Resources/primitives.json", json);
        }
    }
}
