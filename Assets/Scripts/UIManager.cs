﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class UIManager : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        private Text valueTextBox;
#pragma warning restore 0649

        private void Start()
        {
            if (valueTextBox)
            valueTextBox.text = Resources.Load<GameData>("GameData").ObsevableTime.ToString();
        }
    }
}
