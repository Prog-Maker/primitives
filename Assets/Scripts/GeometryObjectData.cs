﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "GeometryObjectData", menuName = "Create GeometryObjectData")]
    public class GeometryObjectData : ScriptableObject
    {
        public List<ClickColorData> ClickColorDatas = new List<ClickColorData>();
    }

    [System.Serializable]
    public class ClickColorData
    {
        public string ObjectType;  //тип создаваемого объекта (куб, сфера, капсула)
        public int MinClicksCount;
        public int MaxClicksCount;
        public Color Color;
    }
}
