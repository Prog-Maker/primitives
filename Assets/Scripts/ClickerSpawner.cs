﻿using UnityEngine;

namespace Game
{
    public class ClickerSpawner : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        private GameObject adminPanel;
#pragma warning restore 0649

        private Camera cam;

        private Primitive[] primitives;

        private void Start()
        {
            cam = Camera.main;

            var textAsset = Resources.Load<TextAsset>("primitives") as TextAsset;

            primitives = JsonHelper.FromJson<Primitive>(textAsset.text);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePos = Input.mousePosition;

                Ray ray = cam.ScreenPointToRay(new Vector3(mousePos.x, mousePos.y, cam.nearClipPlane));

                if (Physics.Raycast(ray, out var hit, 20f))
                {
                    if (hit.collider != null)
                    {
                        TrySetClickToPrimitive(hit.collider);
                    }
                }
                else
                {
                    var pos = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

                    InstatiatePrimitive(pos);
                }

            }
        }


        private void InstatiatePrimitive(Vector3 pos)
        {
            var index = Random.Range(0, primitives.Length);

            var primitive = Resources.Load<GameObject>(primitives[index].Name);

            if (primitive)
            {
                if (!adminPanel.activeSelf) adminPanel.SetActive(true);
                
                var go = Instantiate(primitive, pos, Quaternion.identity);
                go.name = primitives[index].Name;
            }
        }

        private void TrySetClickToPrimitive(Collider collider)
        {
            GeometryObjectModel gom = collider.GetComponent<GeometryObjectModel>();

            if (gom)
            {
                gom.SetClick();
            }
        }
    }

    [System.Serializable]
    public class Primitive
    {
        public string Name;
    }

    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
}