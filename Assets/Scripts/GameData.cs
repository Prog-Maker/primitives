﻿using UnityEngine;

[CreateAssetMenu(fileName ="GameData",menuName = "Create Game Data")]
public class GameData : ScriptableObject
{
    public int ObsevableTime = 2;
}
